# ATM

Python Project: 4/13-14/2021

ATM project

Create a client that takes in a four digit pin to access an account and a password, encrypts the password and sends the pin and password to the Server.

Create a server that receives the pin and password. Decrypts the password and compares it to the correct password as well as making sure the pin is correct in order to allow access to the account. 

The program should catch for invalid passwords and pins and allow the user a max of three times to validate credentials. 

Once credentials are validated the server sends the amount on the account and then provide options on what they want to do: Deposit, withdrawal or just display their account. 

MVP:
1) send something encrypted - REACHED
2) send something clear text (the pin) - REACHED
3) compare something against known information - REACHED
4) have the server provide options on what todo next - REACHED

**Bonus if they have extra time**
b1) option for user to change the password
b2) option for user to change the pin
b3) complete the process of actually taking out/depositing/transferring money between accounts (I.E send money from saving to checking) and store this information persistently
