# menu template 

class atm_menu():
    def __init__(self, balance, password, pin):
        self.total = balance
        self.password = password
        self.pin = pin
        self.displayOptions()

    again = "y"

    def option1(self):
        #input handling 
        while True:
            try:
                #Ask user to enter deposit amount 
                #Save deposit amount 
                depositAmount = input("Enter deposit amount: ")
                float(depositAmount)
                break
    
            except ValueError:
                print("Not a float. Try again.")
            
        #Add saved amount to total 
        self.total = self.total + float(depositAmount)
        print(self.total)

    def option2(self):
        #input handling
        while True:
            try:
                #Ask user to enter withdraw amount 
                #Save withdraw amount 
                withdrawAmount = input("Enter withdraw amount: ")
                float(withdrawAmount)
                break

            except ValueError:
                print("Not a float. Try again.")

        #Subtract saved amount from total 
        self.total = self.total - float(withdrawAmount)
        print(self.total)
        
    def option3(self):
        #print(money)
        #Display account pin
        print(self.pin)
        #Display account password
        print(self.password)
        #Display total amount in account
        print("Total amount in account: $", format(self.total, ",.2f"), sep='')

    def displayOptions(self):
        while self.again.lower() == "y":  

            #display options menu
            print("\n------------------ATM Menu------------------")
            print("\n1: Deposit")
            print("2: Withdraw")
            print("3: Display Account")

            #prompt user to choose an option
            button = input("\nWhat choice do you select (1,2,3): ")
            #1st option
            if (button == '1'):
                self.option1()
            #2nd option
            elif (button == '2'):
                self.option2()
            #3rd option
            elif (button == '3'):
                self.option3()
            else:
                print("\nYou have entered an incorrect option.")
        
            self.again = input("\nKeep accesing menu? (y/n): ") or "n"
    
