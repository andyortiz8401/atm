import socket 
import base64
import menu_template

# Entering a password
def passwordIn(password, pin):
    

    # Combining pass and pin into a tuple, converting to a string and encoding
    combo = (password, pin)
    combostr = ','.join(combo)
    code = combostr.encode('utf-8')

    # Encrypt message in base64 and sending to the server
    encrypt = base64.b64encode(code)
    s.send(encrypt)

# Create the server socket, connect to the server, and recieve the welcome message
s = socket.socket()
s.connect(('127.0.0.1', 5000))
print(s.recv(1024).decode())

count = 3

while count > 0:

    accountname = ""
    password = ""
    pin = ""
    access = False
    balance = 0

    accountname = input("Enter account name: ")
    s.send(accountname.encode('utf-8'))

    # print("You have", count, "tries left.")
    password = input("Enter Password: ")
    # pin = input("Enter PIN: ")

    while True:
        
        pin = input("Enter PIN: ")
        if (pin.isnumeric() == True) and (len(pin) == 4):
            break
        else:
            print("Entered pin incorrectly (Must be four digits). Try again.")

    passwordIn(password, pin)

    access = s.recv(1024).decode('utf-8')
    print(access)

    if access == "True":
        balance = float(s.recv(64).decode('utf-8'))
        print(balance)
        menu_template.atm_menu(balance, password, pin)
        break

    else:
        count -= 1
        print("Wrong password or pin, try again")
        countMessage = s.recv(1024).decode('utf-8')
        print(countMessage)

        

s.close
