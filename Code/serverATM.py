import socket
import base64

# Create the Server Socket, bind it to the loopback ip, and listen for a connection
s = socket.socket()
s.bind(('127.0.0.1', 5000))
s.listen()

print('Listening for connections')

# Create preset data to compare the recieved Passwords and Pins
class Account:
    balance = 0.0
    password = ''
    pin = 0000

accountTest = Account()
accountTest.balance = 50
accountTest.password = "test"
accountTest.pin = 1234

account1 = Account()
account1.balance = 75
account1.password = 'andyortiz'
account1.pin = 4109

dictAccounts = {
    "test" : accountTest,
    "andy" : account1
}

# When a password and pin is entered, compare it to the dictionary of preset passwords and pins
def compare(enteredaccount, enteredpass, enteredpin):
    try:
        # Grab the correct pass and pin combination 
        correctPin = dictAccounts[enteredaccount].pin
        correctPass = dictAccounts[enteredaccount].password
    except:
        print("The account is incorrect.")
        return False
    if enteredpin == correctPin and enteredpass == correctPass:
        print("Access granted")
        return True
    else:
        return False


def passwordOut():
    pswrd = c.recv(64)
    codedPass = base64.b64decode(pswrd)
    decoded = codedPass.decode('utf-8')
    return decoded

# When a connection has been made, print ip address of the client, and send welcoming message
while True:

    # count = 3

    # while count >= 0:

    c, addr = s.accept()
    print("Connected with ", addr)
    c.send(bytes('Welcome','utf-8'))

    count = 3

    while count > 0:

        accountname = ""
        combo = ("", "")
        password = ""
        pin = 0000

        accountname = c.recv(64).decode('utf-8')
        
        # Recieving password and pin
        combo = passwordOut().split(',')
        #print(combo)
        password = combo[0]
        pin = int(combo[1])
        # print(pin)

        # Comparing Password and Pin
        print(compare(accountname, password, pin))
        if compare(accountname, password, pin) == True:
            # Menu options for an account
            print("entered")
            c.send(bytes(str(True), 'utf-8'))
            c.send(bytes(str(dictAccounts[accountname].balance), 'utf-8'))
            # break
            exit()
        else:
            count -= 1
            message = f"You have {count} tries left."
            c.send(bytes(str(False), 'utf-8'))
            c.send(bytes(str(message), 'utf-8'))

            # c.send(bytes("Wrong password or pin, try again",'utf-8'))

    c.close()
